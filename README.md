# Nextcloud compose stack

- basic docker compose stack
- containers : 
  - nextcloud
  - nginx
  - mariadb

## directory structure to create

```
mkdir -p {data,html,logs,ssl}
```

## Permissions

```
chown -R www-data: ./html
chown -R www-data: ./data
```